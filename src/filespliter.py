'''
Created on Nov 9, 2014

@author: squall
'''

import codecs
import os
from marc.marc2tsv import correctYearInfo


def partition(input_path, size):
    fin = codecs.open(input_path, encoding = 'utf-8')
    partition_no = 0
    output_path = input_path + '.' + str(partition_no)
    fout = codecs.open(output_path, encoding = 'utf-8', mode = 'a')
    
    lineno = 0
    for line in fin:
        if len(line) > 1 or line is not None:
            line = correctYearInfo(line)
            fout.write(line)
            lineno += 1
            if (lineno % 100000) == 0:
                print 'Partitioning ' + str(lineno) + ' records.'
                out_size = os.stat(output_path).st_size
                if out_size > size:
                    partition_no += 1
                    output_path = input_path + '.' + str(partition_no)
                    fout.close()    # close the old writer
                    fout = codecs.open(output_path, encoding = 'utf-8', mode = 'a') # open a new writer to a new output file
    print '[Done]'