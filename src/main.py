'''
Created on Oct 19, 2014

@author: squall
'''
from os import listdir
from re import search
from filespliter import partition

''' if __name__ == '__main__':
    pass
'''

from marc.marc2tsv import marc2tsv_runner
import os

src_dir = '/Users/squall/Desktop/lib_dataset/havard_data/hlom/'

# file_list = filter(lambda x : search('.mrc', x), listdir(src_dir))
# statinfo = os.stat(src_dir + 'book.tsv')
# print statinfo.st_size

# assignID = 1
# for seri in file_list:
#     assignID = marc2tsv_runner(src_dir + seri, src_dir + 'book.tsv', assignID)

book_location = src_dir + 'book.tsv'

partition(book_location, 100000000)

# test_string = 'Hello world!!! ' + '\t' + 'Done'
# print test_string
# print test_string.index('\t')
