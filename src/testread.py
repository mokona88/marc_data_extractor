'''
Created on Oct 30, 2014

@author: squall
'''
import csv;
from pymarc import MARCReader;
from pymarc import field
from pymarc.field import Field
from os import listdir;
from re import search;
src_dir = '/Users/squall/Desktop/lib_dataset/havard_data/hlom/'

# get a list of all .mrc files in source directory
file_list = filter(lambda x: search('.mrc', x), listdir(src_dir));
# for seri in file_list:
#     print seri;

# write the output to the same folder as source
# csv_out = csv.writer(open( src_dir + 'cambridge.csv', 'w'), delimiter = '\t', quotechar='"', quoting = csv.QUOTE_MINIMAL)

# create the header row
# csv_out.writerow(['publication_title', 'print_identifier', 'online_identifier', 'date_first_issue_online',
#                   'num_first_vol_online','num_first_issue_online','date_last_issue_online','num_last_vol_online',
#                   'num_last_issue_online','title_url','first_author','title_id','coverage_depth','coverage_notes',
#                   'publisher_name','location','title_notes', 'oclc_collection_name','oclc_collection_id','oclc_entry_id',
#                   'oclc_linkscheme', 'oclc_number', 'action']);
 
for item in file_list:
    fd = file(src_dir + item, 'r');
    reader = MARCReader(fd, to_unicode=True);
    for record in reader:
#         publication_title = print_identifier = online_identifier = date_first_issue_online = num_first_vol_online = num_first_issue_online = date_last_issue_online = num_last_vol_online = num_last_issue_online = title_url = first_author = title_id = coverage_depth = coverage_notes = publisher_name = location = title_notes = oclc_collection_name = oclc_collection_id = oclc_entry_id = oclc_linkscheme = oclc_number = action = '';
        result = '';
#         if record.title() is not None:
#             result += record.title()
#         else:
#             result += 'NULL'
#         if record.isbn() is not None:
#             result += '\t' + record.isbn()
#         else:
#             result += '\tNULL'
#         if record.author() is not None:
#             result += '\t' + record.author()
#         else:
#             result += '\tNULL'
#         if record.uniformtitle() is not None:
#             result += '\t' + record.uniformtitle()
#         else:
#             result += '\tNULL'
        series_tmp = record.series();
        if record.series() is not None:
            for seri in record.series():
                if seri.tag is not None:
                    result += '\t' + seri.tag;
                if seri.indicators is not None:
                    for indicator_item in seri.indicators:
                        result += '\t' + str(indicator_item)
                if seri.subfields is not None:
                    arrsize = seri.subfields.__len__()
                    for i in range (0, arrsize):
                        key = seri.subfields[i]
                        result += '\t' + key
        else:
            result += '\tNULL'
#  
#         added_entries = record.addedentries()
#         if record.addedentries() is not None:
#             for seri in record.addedentries():
#                 result += '\t' + seri.tag
#         else:
#             result += '\tNULL'
# 
#         if record.location() is not None:
#             for i in range (0, record.location().__len__()):
#                 result += record.location()[i]
#         else:
#             result += '\tNULL'
#         if record.notes() is not None:
#             notes = record.notes()
#             for note in record.notes():
#                 result += '\t' + str(note)
#         else:
#             result += '\tNULL'
#             
#         if record.physicaldescription() is not None:
#             phys = record.physicaldescription()
#             for seri in record.physicaldescription():
#                 result += '\t' + str(seri)
#         else:
#             result += '\tNULL'
#         if record.publisher() is not None:
#             result += '\t' + record.publisher()
#         else:
#             result += '\tNULL'
#         if record.pubyear() is not None:
#             result += '\t' + record.pubyear()
#         else:
#             result += '\tNULL'
#         phys_desc = record.physicaldescription()
#         field_entry = phys_desc[0]
#         a = True
#         print a
#         if type(phys_desc[0]) is Field:
#             print True
        
        if result != '':
            print result
#         break;
    break;
#         if record['856'] is not None:
#             title_url = record['856']['u'];

