'''
Created on Nov 3, 2014

@author: squall
'''

from pymarc import MARCReader
from os import path
import csv
from pymarc.field import Field
import codecs
import io
# from pymarc import field

def marc2tsv_runner(input_path, output_path, assignID):
#{
    """
        Extract data from MARC dataset (single mrc file)
        input_path: path to mrc file
        output_path: path to csv file
        Records are appended if output files are existed
    """
    recordID = assignID
    print '[Processing] ... ' + input_path
    print 'Extracting records...'
    # create record file to store main outputs
    existed = False
    if path.isfile(output_path):
        existed = True
    out_file = open(output_path, 'a')
    out_file.write(codecs.BOM_UTF8)
    csv_out = csv.writer(out_file, delimiter = '\t', quotechar='"', quoting = csv.QUOTE_MINIMAL)
    # create relational files to store relational data
    print 'Creating records for series...'
    series_record = output_path + '.series'
            
    # create the header row
    if not existed:
        csv_out.writerow(['infoID', 'title', 'isbn', 'author', 'uniform_title', 'location', 'notes', 'physical_description', 'added_entries', 'publisher', 'pub_year']);
    pFile = file(input_path, 'r')
    reader = MARCReader(pFile, to_unicode = True, force_utf8 = True)
    line = 0

    try:
        for record in reader:
    #   {
            title = isbn = author = uniformTitle = location = notes = ''
            physical_description = added_entries = publisher = pub_year = ''
            
            if record.title() is not None:
                title = record.title()
#                 title = blank2Null(title)
            else:
                title = 'NULL'
            if record.isbn() is not None:
                isbn = record.isbn()
#                 isbn = blank2Null(isbn)
            else:
                isbn = 'NULL'
            if record.author() is not None:
                author = record.author()
#                 author = blank2Null(author)
            else:
                author = 'NULL'
            if record.uniformtitle() is not None:
                uniformTitle = record.uniformtitle()
#                 uniformTitle = blank2Null(uniformTitle)
            else:
                uniformTitle = 'NULL'
            if record.series() is not None:
                extractSeries(record, series_record, recordID)
    #             extractField(record, record.series(), series_record)
            if record.addedentries() is not None:
                added_entries = list2String(record.addedentries())
#                 added_entries = blank2Null(added_entries)
            if record.location() is not None:
                location = list2String(record.location())
#                 location = blank2Null(location)
            if record.notes() is not None:
                notes = list2String(record.notes())
#                 notes = blank2Null(notes)
            if record.physicaldescription() is not None:
                physical_description = list2String(record.physicaldescription())
#                 physical_description = blank2Null(physical_description)
            if record.publisher() is not None:
                publisher = record.publisher()
            else:
                publisher = 'NULL'
            if record.pubyear() is not None:
                pub_year = record.pubyear()
                pub_year.replace('[', '')
                pub_year.replace(']', '')
            else:
                pub_year = 'NULL'
            csv_out.writerow([unicode(recordID).encode('utf-8'),unicode(title).encode('utf-8'), unicode(isbn).encode('utf-8'), unicode(author).encode('utf-8'), unicode(uniformTitle).encode('utf-8'), 
                              unicode(location).encode('utf-8'), unicode(notes).encode('utf-8'), unicode(physical_description).encode('utf-8'), unicode(added_entries).encode('utf-8'), unicode(publisher).encode('utf-8'),
                              unicode(pub_year).encode('utf-8')])
            line += 1
            recordID += 1
            if line % 100000 == 0:
                print '... ' + str(line) + ' records has been read.'
    except UnicodeDecodeError:
        print 'Error occurs at line ' + str(line)
        pass
#   }
    print '[Done]'
    return recordID
#}

def extractSeries(record, output, recordID):
    series_list = record.series()
    if series_list.__len__() > 1:
        for i in range(0, series_list.__len__()):
            extractField(recordID, series_list[i], output)

def extractField(recordID, field, output):
# {
    existed = False;

    if path.isfile(output):
        existed = True
    
    out_file = open(output, 'a')
    out_file.write(codecs.BOM_UTF8)
    csv_driver_record = csv.writer(out_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    if not existed:
        csv_driver_record.writerow(['recordID', 'tag', 'indicators', 'subfields'])
    if field.indicators.__len__() < 0:
        indicators = 'NULL'
    else:
        indicators = field.indicators[0] + ' ' + field.indicators[1]
    subfields = list2String(field.subfields)
    csv_driver_record.writerow([unicode(recordID).encode('utf-8'), unicode(field.tag).encode('utf-8'),
                                unicode(indicators).encode('utf-8'), unicode(subfields).encode('utf-8')])
# }

def list2String(list):
    length = list.__len__()
    return_val = ''
    if length < 1:
        return 'NULL'
    else:
        for i in range(0, length):
            if type(list[i]) is Field:
                return_val = field2String(list[i])
            else:
                if len(return_val) < 1:
                    return_val += list[i]
                else:
                    return_val += ',' + list[i]
        return return_val
    return None

def field2String(field):
    if field.subfields.__len__() > 2:
        return field.subfields[1] + ' ' + field.subfields[3]
    elif field.subfields.__len__() > 0:
        return field.subfields[1]
    else:
        return 'NULL'
        
def blank2Null(entry):
    if len(entry) < 1:
        return 'NULL'
    else:
        return entry
    return None

def correctYearInfo(line):
    length = line.__len__()
    sub_str = line[length - 20 :]
    tab_pos = 0
    try:
        tab_pos = sub_str.index('\t')
        if (tab_pos < 0):
            tab_pos = 0
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace('[','')
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace(']','')
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace('?','')
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace('-','')
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace('.','')
        sub_str = sub_str[:tab_pos] + sub_str[tab_pos:].replace('c','')
    except ValueError:
        pass
    line = line[:length - 10] + sub_str
    return line
    